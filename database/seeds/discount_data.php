<?php

use Illuminate\Database\Seeder;

class discount_data extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
	    	DB::table('discount')->insert([
	            'discount_value' => 50,
	            'no_of_time' => 15,
            ]);
            
            DB::table('discount')->insert([
	            'discount_value' => 100,
	            'no_of_time' => 12,
            ]);
            
            DB::table('discount')->insert([
	            'discount_value' => 200,
	            'no_of_time' => 10,
            ]);
            
            DB::table('discount')->insert([
	            'discount_value' => 500,
	            'no_of_time' => 8,
            ]);
            
            DB::table('discount')->insert([
	            'discount_value' => 1000,
	            'no_of_time' => 5,
            ]);
            
            DB::table('discount')->insert([
	            'discount_value' => 2000,
	            'no_of_time' => 4,
            ]);
            
            DB::table('discount')->insert([
	            'discount_value' => 5000,
	            'no_of_time' => 2,
            ]);
            
            DB::table('discount')->insert([
	            'discount_value' => 10000,
	            'no_of_time' => 1,
	        ]);
    
    }
}
