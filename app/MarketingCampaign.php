<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingCampaign extends Model
{
    protected $table = 'marketing_campaign';

    protected $fillable = [
        'first_name', 
        'last_name', 
        'email','mobile',
        'discount_value',
        'ip_address',
    ];

}
