<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Response;

class MarketingCampaignCtrl extends Controller
{
    public function form_data(Request $request){
       //  return $request->email;
        $check = \App\MarketingCampaign::where('email', $request->email)->orWhere('mobile', $request->mobile)->first();
        $discount = \App\Discount::all()->random(1)->first();

        if(isset($discount->no_of_time)){
            if($discount->no_of_time > 0){
                $discount_val = $discount->discount_value;
                $discount->no_of_time = $discount->no_of_time - 1;    
                $discount->update();
            }else{
                $discount_val = 0;
            }
        }else{
            $discount_val = 0;
        }

       if(!$check){
            $savedata = new \App\MarketingCampaign;
            $savedata->first_name = $request->first_name;
            $savedata->last_name = $request->last_name;
            $savedata->email = $request->email;
            $savedata->mobile = $request->mobile;
            $savedata->discount_value = $discount_val;
            $savedata->save();
            if($discount_val==0){
                return Response::json(['message_response'=>'Hi '. $request->first_name.' ,no more discount values left']);
            }else{
                return Response::json(['message_response'=>'Congratulations, you’ve won '.$discount_val.' Rs.']);
            }
        }else{
            return Response::json(['message_response'=>'Hi '. $request->first_name.' , you’ve already given a discount value']);
        }
        
    }
}
